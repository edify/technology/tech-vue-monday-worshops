import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CreateView from '../views/CreateView.vue'
import MyBlogView from '@/views/MyBlogView.vue'
import PostView from '@/views/PostView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/create',
      name: 'create',
      component: CreateView
    },
    {
      path: '/my-blog',
      name: 'My Blog',
      component: MyBlogView
    },
    {
      path: '/post/:id',
      name: 'Post',
      component: PostView
    },
  ]
})

export default router
