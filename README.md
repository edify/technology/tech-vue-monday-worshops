# Vue Monday Workshops

## Recordings

- [January 15th, 2024: Workshop 1.0](https://drive.google.com/file/d/1qsV6M1a6G4Vs9iSH-XxqtQd_dRKySLjk/view?usp=sharing)
- [January 22th, 2024: Workshop 1.1](https://drive.google.com/file/d/1UytjwCzncl8RYk8b40h6k7Blf6jLjttx/view?usp=sharing)
- [January 29th, 2024: Workshop 2.0: Slots and Component Libraries](https://drive.google.com/file/d/1_vDDS87CFLlCnvcextjUzy4XraO-0nMo/view?usp=sharing)
- [February 12th, 2024: Workshop 2.1: Provide/Inject](https://drive.google.com/file/d/1yKt5RnD0XxaefP2RzRkyotwGVeNP3oUW/view?usp=sharing)

## Workshop 1: Introduction to Vue.js and Setup

### Objectives

1. Introduce Vue.js and its core concepts.
2. Set up a development environment.

### Agenda

1. Introduction and Project Overview (1 hour)
    - Overview of the [API resources](https://jsonplaceholder.typicode.com/).
    - Goals of the project.
2. Introduction to Vue.js
    - What is [Vue.js](https://vuejs.org/guide/introduction.html#what-is-vue)?
    - [Single file](https://vuejs.org/guide/introduction.html#single-file-components) components
    - [Options API](https://vuejs.org/guide/introduction.html#options-api)
    - [Composition API](https://vuejs.org/guide/introduction.html#composition-api)
    - Components: creating reusable UI elements
3. Getting Started
  4. Setting up the [development environment](https://vuejs.org/guide/quick-start.html#creating-a-vue-application) by creating a basic Vue.js project with Vite
  5. Understanding the [project structure](https://vuex.vuejs.org/guide/structure.html).
  6. Creating a [Vue instance](https://vuejs.org/guide/essentials/application.html).
  7. Basic syntax:
     - [Templating](https://vuejs.org/guide/essentials/template-syntax.html)
     - [Directives](https://vuejs.org/guide/essentials/template-syntax.html#directives)
     - [Data binding](https://vuejs.org/guide/essentials/template-syntax.html#attribute-bindings)
     - [Class and Style Bindings](https://vuejs.org/guide/essentials/class-and-style)
     - [Conditional Rendering](https://vuejs.org/guide/essentials/conditional)
     - [List Rendering](https://vuejs.org/guide/essentials/list)
     - [Event Handling](https://vuejs.org/guide/essentials/event-handling)
     - [Form Input Bindings](https://vuejs.org/guide/essentials/forms)
     - [Lifecycle Hooks](https://vuejs.org/guide/essentials/lifecycle)
     - [Template Refs](https://vuejs.org/guide/essentials/template-refs)
     - [Components Basics](https://vuejs.org/guide/essentials/component-basics)
4. [Reactivity](https://vuejs.org/guide/essentials/reactivity-fundamentals.html) fundamentals


## Workshop 2: Components and Props


### Objectives

1. Understand and work with Vue components.
2. Learn how to communicate between components using props.


### Agenda

1. Components In-Depth
  1. [Registration](https://vuejs.org/guide/components/registration)
  2. [Props](https://vuejs.org/guide/components/props)
  3. [Events](https://vuejs.org/guide/components/events)
  4. [Component v-model](https://vuejs.org/guide/components/v-model)
  5. [Fallthrough Attributes](https://vuejs.org/guide/components/attrs)
  6. [Slots](https://vuejs.org/guide/components/slots)
  7. [Provide / inject](https://vuejs.org/guide/components/provide-inject)
  8. [Async Components](https://vuejs.org/guide/components/async)

### Workshop 3: State Management with Pinia

## Objective:

* Introduce state management with Pinia.
* Implement interactive features in the application.

## Agenda:

1. Introduction to Pinia:
  * Why and when to use Pinia.
  * Setting up a Pinia store.
2. Managing State:
  * Storing and updating state in Pinia.
  * Actions for API interactions.
3. Adding Interactivity:
  * Implementing features like adding comments or todos.
  * Updating and synchronizing state.
4. Hands-on Exercise:
  * Participants integrate Pinia for state management.
  * Implement interactive features.
  * Q&A session.


## Workshop 4: Routing and Advanced Topics

### Objective

* Explore advanced Vue.js features, including routing.

### Agenda

1. [Vue Router](https://vuejs.org/guide/introduction.html#composition-api:~:text=Tooling-,Routing,-State%20Management) :
  * Setting up Vue Router.
  * Navigation in a Vue.js app.
2. [Computed Properties](https://vuejs.org/guide/essentials/computed) and [Watchers](https://vuejs.org/guide/essentials/watchers)
  * Understanding computed properties.
  * Using watchers for reactive programming.
3. Reusability
  * [Composables](https://vuejs.org/guide/reusability/composables)
  * [Custom Directives](https://vuejs.org/guide/reusability/custom-directives)
  * [Plugins](https://vuejs.org/guide/reusability/plugins)
4. Built-in components
  * Transition
  * TransitionGroup
  * KeepAlive
  * Teleport
  * Suspense

## Workshop 5: Testing and Best Practices

### Objectives


### Agenda

1. [Testing](https://vuejs.org/guide/scaling-up/testing.html)

2. Best Practices
  * [Reactivity Debugging](https://vuejs.org/guide/extras/reactivity-in-depth.html#reactivity-debugging)
  * Performance
    * [SSR](https://vuejs.org/guide/scaling-up/ssr.html), [SSG](https://vuejs.org/guide/scaling-up/ssr.html#ssr-vs-ssg)
  * [Accessibility](https://vuejs.org/guide/best-practices/accessibility.html)
  * [Security](https://vuejs.org/guide/best-practices/security.html)

